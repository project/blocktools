
BlockTools provides a simple programatic interface for retrieving individual 
blocks from the block system while retaining Drupal's block caching mechanism.
Additionally, BlockTools provides per block caching that can be configured 
through each block's configuration form.  

To invoke blocktools, simply call it like so:

  blocktools_get_block($module, $delta, $cache_skip)

Where module and delta string representations of the module providing the block
and the block name (the delta) and an option third argument which, if true, 
skips all block caching mechanisms.  This returns the entire block array 
returned when providing hook_block with the 'view' operation argument.  An 
additional 'cached_result' key is added to the block array if the block was 
retreived from the cache.

You may also call blocktools_get_block_content($module, $delta, $cache_skip) if
you all you want is a block body.


How does BlockTools interact with Drupal's block cache?

* If global caching is turned off, and a block's individual cache is turned on, 
  caching will be used for that block.

* If global caching is enabled, anything cached by the traditional block system 
  will be shared with blocktools, and vice versa.  

* Currently, if the expiration set by the block in hook_block varies from the 
  administrator-set expiration, the expiration will be set by the version that 
  gets called first.  This is a bug, and it will be fixed in a subsequent 
  release.

* Calling up blocks with BlockTools always adds a little overhead compared to 
  calling them directly.  This potentially affects Views-generated blocks and 
  very simple blocks if caching is enabled for those blocks.  The overhead is 
  not significant, and potentially the module will include a short circuit for
  views-provided blocks.  


--------------

BlockTools development is supported by the Chicago Technology Cooperative (http://chicagotech.org)
